import sys
from itertools import cycle
from pathlib import Path
from textwrap import dedent


def calibration(input_iterable, start_value=0):
    t = [int(i) for i in input_iterable]
    t.append(start_value)
    return sum(t)


def find_repeat(input_string):
    present_sum = 0
    all_sums = {present_sum}
    for i in map(int, cycle(input_string)):
        present_sum += i
        if present_sum in all_sums:
            return present_sum

        all_sums.add(present_sum)


def main(filename):
    base_dir = Path('.')
    with open(str((base_dir / filename).resolve()), 'r') as f:
        data = f.read()

    part_1 = calibration(data.splitlines())
    part_2 = find_repeat(data.splitlines())

    return part_1, part_2


if __name__ == '__main__':
    first_result, second_result = main(sys.argv[1])
    result = dedent(f'Part 1 Day 1: {first_result}\n\n'
                    f'Part 2 Day 1: {second_result}\n\n')
    sys.stdout.write(result)
