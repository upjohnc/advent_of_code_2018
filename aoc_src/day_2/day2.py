from collections import Counter
import sys


def count_of_instances(input_string, instance_value):
    count_characters = Counter(input_string)
    count_instances = Counter(count_characters.values())
    return count_instances.get(instance_value, 0)


def check_two_count(input_string):
    return 0 if count_of_instances(input_string, 2) == 0 else 1


def check_three_count(input_string):
    return 0 if count_of_instances(input_string, 3) == 0 else 1


def sum_two_count(input_string):
    return sum(map(check_two_count, input_string.splitlines()))


def sum_three_count(input_string):
    return sum(map(check_three_count, input_string.splitlines()))


def part1_(input_string):
    two_count = sum_two_count(input_string)
    three_count = sum_three_count(input_string)
    return two_count * three_count


if __name__ == '__main__':
    filename = sys.argv[1]
    with open(filename, 'r') as f:
        text = f.read()
    part_1 = part1_(text)
    sys.stdout.write(f'Part 1: {part_1}\n\n')
