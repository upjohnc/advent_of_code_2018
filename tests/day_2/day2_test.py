from aoc_src.day_2 import day2
import pytest

# abcdef contains no letters that appear exactly two or three times.
# bababc contains two a and three b, so it counts for both.
# abbcde contains two b, but no letter appears exactly three times.
# abcccd contains three c, but no letter appears exactly two times.
# aabcdd contains two a and two d, but it only counts once.
# abcdee contains two e.
# ababab contains three a and three b, but it only counts once.
input_strings = ('abcdef', 'bababc', 'abbcde', 'abcccd', 'aabcdd', 'abcdee', 'ababab')
two_count_result = (0, 1, 1, 0, 1, 1, 0)
three_count_result = (0, 1, 0, 1, 0, 0, 1)


@pytest.mark.parametrize('input_string, expected', tuple(zip(input_strings, two_count_result)))
def test_count_of_two_count(input_string, expected):
    result = day2.check_two_count(input_string)
    assert result == expected


@pytest.mark.parametrize('input_string, expected', tuple(zip(input_strings, three_count_result)))
def test_count_of_three_count(input_string, expected):
    result = day2.check_three_count(input_string)
    assert result == expected


def test_sum_two_count():
    input_text = '\n'.join(input_strings)
    expected = sum(two_count_result)
    result = day2.sum_two_count(input_text)
    assert result == expected


def test_sum_three_count():
    input_text = '\n'.join(input_strings)
    expected = sum(three_count_result)
    result = day2.sum_three_count(input_text)
    assert result == expected
