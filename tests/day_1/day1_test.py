import pytest

from aoc_src.day_1 import day1

input_data_zero = (('+1', '+1', '+1'), ('+1', '+1', '-2'), ('-1', '-2', '-3'))
input_data_three = (('+1', '+1', '+1'), ('+1', '+1', '-2'), ('-1', '-2', '-3'))
result_data = ((3, 0, -6), (6, 3, -3))
data_zero = zip(input_data_zero, result_data[0])
data_three = zip(input_data_three, result_data[1])


@pytest.mark.parametrize('input_value, expected', data_zero)
def test_starting_point_zero(input_value, expected):
    """
    GIVEN: a starting point of zero
    WHEN: running calibration function
    THEN: result should assert the correct integer
    """
    t = day1.calibration(input_value)
    assert t == expected


@pytest.mark.parametrize('input_value, expected', data_three)
def test_starting_point_three(input_value, expected):
    """
    GIVEN: a starting point of 3
    WHEN: calling the calibration function
    THEN: result should assert the correct integer
    """
    t = day1.calibration(input_value, start_value=3)
    assert t == expected


data_input = ((["+1", "-1"], 0), (["+3", "+3", "+4", "-2", "-4"], 10), (["-6", "+3", "+8", "+5", "-6"], 5), (["+7", "+7", "-2", "-7", "-4"], 14),)


@pytest.mark.parametrize('input_string, expected', data_input)
def test_find_repeat(input_string, expected):
    result = day1.find_repeat(input_string)
    assert result == expected
